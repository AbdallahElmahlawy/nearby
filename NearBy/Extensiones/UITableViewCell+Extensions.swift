//
//  UITableViewCell+Extensions.swift
//  NearBy
//
//  Created by Abdallah Elmahlawy on 7/19/20.
//  Copyright © 2020 Abdallah Elmahlawy. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell{
    
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib : UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
}

