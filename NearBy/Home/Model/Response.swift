/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Response : Codable {
	let suggestedFilters : SuggestedFilters?
	let suggestedRadius : Int?
	let headerLocation : String?
	let headerFullLocation : String?
	let headerLocationGranularity : String?
	let totalResults : Int?
	let suggestedBounds : SuggestedBounds?
	let groups : [Groups]?

	enum CodingKeys: String, CodingKey {

		case suggestedFilters = "suggestedFilters"
		case suggestedRadius = "suggestedRadius"
		case headerLocation = "headerLocation"
		case headerFullLocation = "headerFullLocation"
		case headerLocationGranularity = "headerLocationGranularity"
		case totalResults = "totalResults"
		case suggestedBounds = "suggestedBounds"
		case groups = "groups"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		suggestedFilters = try values.decodeIfPresent(SuggestedFilters.self, forKey: .suggestedFilters)
		suggestedRadius = try values.decodeIfPresent(Int.self, forKey: .suggestedRadius)
		headerLocation = try values.decodeIfPresent(String.self, forKey: .headerLocation)
		headerFullLocation = try values.decodeIfPresent(String.self, forKey: .headerFullLocation)
		headerLocationGranularity = try values.decodeIfPresent(String.self, forKey: .headerLocationGranularity)
		totalResults = try values.decodeIfPresent(Int.self, forKey: .totalResults)
		suggestedBounds = try values.decodeIfPresent(SuggestedBounds.self, forKey: .suggestedBounds)
		groups = try values.decodeIfPresent([Groups].self, forKey: .groups)
	}

}