//
//  HomeVC.swift
//  NearBy
//
//  Created by Abdallah Elmahlawy on 7/19/20.
//  Copyright © 2020 Abdallah Elmahlawy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import CoreLocation
import MapKit
class HomeVC: UIViewController,CLLocationManagerDelegate {
    
    
    @IBOutlet weak var placesTV: UITableView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorViewImg: UIImageView!
    @IBOutlet weak var ErorrLbl: UILabel!
    
    var homeViewModel = HomeViewModel()
    public var items = PublishSubject<[Items]>()
    let locManager = CLLocationManager()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locManager.delegate = self
        placesTV.registerCellNib(cellClass: PlaceTVC.self)
        configureBindings()
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            loadData()
        }else{
            locManager.requestWhenInUseAuthorization()
            showAccessibilityError()
        }
    }
    
    
    // MARK: - Bindings
    private func configureBindings() {
        
        // binding loading to vc
        homeViewModel.loading
            .bind(to: self.rx.isAnimating).disposed(by: disposeBag)
        
        // observing errors to show
        homeViewModel
            .error
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { (error) in
                switch error {
                case .internetError( _):
                    //                    MessageView.sharedInstance.showOnView(message: message, theme: .error)
                    self.showConectionError()
                case .serverMessage(let message):
                    MessageView.sharedInstance.showOnView(message: message, theme: .warning)
                }
            })
            .disposed(by: disposeBag)
        
        // binding items
        homeViewModel
            .items
            .observeOn(MainScheduler.instance)
            .bind(to: self.items)
            .disposed(by: disposeBag)
        
        self.items.bind(to: placesTV.rx.items(cellIdentifier: PlaceTVC.identifier, cellType: PlaceTVC.self)) {  (row,item,cell) in
            cell.placeItem = item
        }.disposed(by: disposeBag)
        
    }
    
    // MARK: - Methods
    func loadData() {
        self.navigationController?.navigationBar.isHidden = false
        errorView.isHidden = true
        guard let currentLocation = locManager.location else {
            return
        }
        var ll = ""
        print(currentLocation.coordinate.latitude)
        print(currentLocation.coordinate.longitude)
        ll = "\(currentLocation.coordinate.latitude),\(currentLocation.coordinate.longitude)"
        homeViewModel.requestData(ll: ll)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .notDetermined:
            locManager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            loadData()
            break
        case .authorizedAlways:
            loadData()
            //            locManager.startUpdatingLocation()
            break
        case .denied:
            showAccessibilityError()
            break
        default:
            break
        }
        
    }
    
    func showConectionError() {
        self.navigationController?.navigationBar.isHidden = true
        errorView.isHidden = false
        errorViewImg.image = UIImage(named: "wifi")
        ErorrLbl.text = "No Internet Connection, Check Your Conectivity"
    }
    
    func showAccessibilityError() {
        self.navigationController?.navigationBar.isHidden = true
        errorView.isHidden = false
    }
    
    // MARK: - Actions
    @IBAction func refreshBtn(_ sender: Any) {
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            loadData()
        }else{
            showAccessibilityError()
            locManager.requestWhenInUseAuthorization()
        }
    }
    
}
