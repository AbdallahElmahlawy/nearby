//
//  PlaceTVC.swift
//  NearBy
//
//  Created by Abdallah Elmahlawy on 7/20/20.
//  Copyright © 2020 Abdallah Elmahlawy. All rights reserved.
//

import UIKit

class PlaceTVC: UITableViewCell {
    
    
    @IBOutlet weak var placeIV: UIImageView!
    
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var addressLbl: UILabel!
    
    var placeItem : Items!{
        didSet{
            titleLbl.text = placeItem.venue?.name
            addressLbl.text = placeItem.venue?.location?.address
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
