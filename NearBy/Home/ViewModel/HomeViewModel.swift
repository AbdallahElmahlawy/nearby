//
//  HomeViewModel.swift
//  NearBy
//
//  Created by Abdallah Elmahlawy on 7/19/20.
//  Copyright © 2020 Abdallah Elmahlawy. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CoreLocation


class HomeViewModel {
    
    let client_id = "B0FWGPS3JGPLBORWIV0GMM1JNRELPBFBONLI032WUDMXRY5G"
    let client_secret = "Z3CRYG3D2HBJE2E0T1NSUCDHDKORWVYMY4V2DR2VA4P2GCB2"
    public enum HomeError {
        case internetError(String)
        case serverMessage(String)
    }
    
    public let groups : PublishSubject<[Groups]> = PublishSubject()
    public let items : PublishSubject<[Items]> = PublishSubject()
    public let loading: PublishSubject<Bool> = PublishSubject()
    public let error : PublishSubject<HomeError> = PublishSubject()
    
    private let disposable = DisposeBag()
    
    
    public func requestData(ll: String){
        self.loading.onNext(true)
        //30.04,31.02
        APIManager.requestData(url: "/explore?ll=\(ll)&client_id=\(client_id)&client_secret=\(client_secret)&v=20200719", method: .get, parameters: nil, completion: { (result) in
            self.loading.onNext(false)
            switch result {
            case .success(let returnJson) :
                print("ok")
                let items = returnJson
                self.items.onNext(items)
                print(items)
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.error.onNext(.internetError("Check your Internet connection."))
                case .authorizationError(let errorJson):
                    self.error.onNext(.serverMessage(errorJson["message"].stringValue))
                default:
                    self.error.onNext(.serverMessage("Unknown Error"))
                }
            }
        })
        
    }
}
